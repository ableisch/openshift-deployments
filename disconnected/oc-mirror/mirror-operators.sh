#! /bin/bash
set -o pipefail
set -o errexit

## Preparation on your registry:
## * create an organization like "oc-mirror"
## * ensure either ~/.docker/config.json or ${XDG_RUNTIME_DIR}/containers/auth.json contains
##   authentication tokens for both upstream and downstream registry.
## * ensure you have a valid session to your OpenShift cluster (as we do some queries)
## Docs: https://github.com/openshift/oc-mirror/tree/main/v2/docs

# Variables to be customized
LOCAL_REGISTRY=<your-registry>/oc-mirror
OC_MIRROR_OPTIONS="--v2"  # while still TP, V2 is required due to changes in the ImageSet API.
OC_MIRROR_WORKSPACE="/tmp/oc-mirror-workspace"
HTTP_PROXY="http://<your-proxy:proxy-port>"
HTTPS_PROXY="$HTTP_PROXY"
NO_PROXY="<your-networks/netmask>,<.yourdomain>"

###############################################################################

function get_catalogs() {
    oc get subscriptions.operators.coreos.com -A  -o jsonpath='{range .items[*]}{.spec.source}{"\n"}{end}' | sort -u 2>/dev/null
}

function get_catalog_image() {
    local CATALOG=${1:-redhat-operators}

    oc get catalogsources.operators.coreos.com -n openshift-marketplace $CATALOG -o jsonpath="{ .spec.image }" 2>/dev/null
}

function get_subscriptions() {
    local CATALOG=${1:-redhat-operators}

    oc get subscriptions.operators.coreos.com -A -o json | jq -r '.items[]| select(.spec.source == "'$CATALOG'")|.metadata.name' | uniq 2>/dev/null
}

function get_subsription_namespace() {
    local SUBSCRIPTION=${1}

    oc get subscriptions.operators.coreos.com -o custom-columns=namespace:metadata.namespace,name:metadata.name -A | awk '/'$SUBSCRIPTION'/ {print $1}'
}

# check openshift login
oc whoami 2>&1 > /dev/null
[ $? -ne 0 ] && echo "You needed to be logged into a cluster" && exit 1

# create workspace dir if missing
[ -d "$OC_MIRROR_WORKSPACE" ] || mkdir -p "$OC_MIRROR_WORKSPACE"

# Replace LOCAL_REGISTRY in our template to mirror-operators.yaml
envsubst '$LOCAL_REGISTRY' < ImageSetConfiguration-header.yaml > mirror-operators.yaml

# fetch list of subscribed operators, strip name from version and remove any version number after the 3rd number
for CATALOG in $(get_catalogs); do
    echo "Processing subscriptions from catalog $CATALOG"
    echo "    - catalog: \"$(get_catalog_image $CATALOG)\"" >> mirror-operators.yaml
    echo "      packages:" >> mirror-operators.yaml
    for SUBSCRIPTION in $(get_subscriptions $CATALOG); do
        for NAMESPACE in $(get_subsription_namespace $SUBSCRIPTION); do
            echo "  Fetching details for $SUBSCRIPTION in $NAMESPACE"
            oc get subs -n $NAMESPACE $SUBSCRIPTION -o go-template-file=get_subscription.gotemplate | sed -re "/minVersion:/ { s/'[^\\.]*\\.v?/'/; s/([0-9]*)\.([0-9]*)\.([0-9]*)\.?\-?.*/\1.\2.\3'/; /<no value>/d}" >> mirror-operators.yaml
        done
    done
done

# make sure proxy settings are used for `oc-mirror`
export http_proxy=$HTTP_PROXY
export https_proxy=$HTTPS_PROXY
export no_proxy=$NO_PROXY

oc-mirror $OC_MIRROR_OPTIONS --config mirror-operators.yaml --workspace "file://$OC_MIRROR_WORKSPACE" "docker://$LOCAL_REGISTRY"
[ $? -ne 0 ] && echo "failed to mirror images" && exit 1

echo "Check ${OC_MIRROR_WORKSPACE}/working-dir/cluster-resources for generated mirror lists"
