#! /bin/bash
# vi: ts=3 sw=3 et
###################################################################################
# Porpose:
#   create Insights collection job as per the documentation - but please check for
#   updated instructions of the documentation
# Author: Andreas Bleischwitz <ableisch@redhat.com>
# License: GPL-v2
# https://docs.openshift.com/container-platform/4.12/support/remote_health_monitoring/remote-health-reporting-from-restricted-network.html

function delete_job_and_pod() {
   INSIGHTS_JOB=$(oc get job --namespace=openshift-insights -o custom-columns=:metadata.name --no-headers --selector job-name=insights-operator-job)                                                                                                                                         
   [ -n "$INSIGHTS_JOB" ] && echo "Deleting existing job" && oc delete job $INSIGHTS_JOB && sleep 5

   INSIGHTS_JOB_POD=$(oc get pods --namespace=openshift-insights -o custom-columns=:metadata.name --no-headers --selector job-name=insights-operator-job)                                                                                                                                    
   [ -n "$INSIGHTS_JOB_POD" ] && echo "Deleting existing completed job" && oc delete pod $INSIGHTS_JOB_POD && sleep 5
}

# Check for valid openshift authentication
OPENSHIFT_USER=$(oc whoami 2>/dev/null)
[ $? -eq 1 ] && echo "You need to be logged into the OpenShift cluster" && exit 1

# fetch auth-token for cloud.redhat.com, cluster-id and insights-operator imagename
TOKEN="$(oc get secret/pull-secret -n openshift-config  -o jsonpath='{.data.\.dockerconfigjson}' | base64 -d | jq -r '.auths."cloud.openshift.com".auth')"                                                                                                                                   
CLUSTER_ID=$(oc get clusterversion -o jsonpath='{.items[].spec.clusterID}')
INSIGHTS_OPERATOR_IMAGE=$(oc get pods -n openshift-insights --selector app=insights-operator -o jsonpath='{.items[0].spec.containers[].image}')                                                                                                                                              

[ -z "$TOKEN" ] && echo "Failed to fetch token from secret/pull-secret" && exit 1
[ -t "$CLUSTER_ID" ] && echo "Failed to fetch cluster-id from clusterversion" && exit 1
[ -z "$INSIGHTS_OPERATOR_IMAGE" ] && echo "Failed to evaluate insights-operator-image" && exit 1

delete_job_and_pod

# Insert Insights-operator image into template and run job
sed -e 's#INSIGHTS_OPERATOR_IMAGE#'$INSIGHTS_OPERATOR_IMAGE'#' < job-template/insights-operator-job.yaml | oc apply -n openshift-insights -f -

# Wait for the job to be completed
COUNTER=39
echo -n "Waiting for insights-operator-job to finish (max " $(( COUNTER * 5 + 10)) " sec): "
sleep 10

INSIGHTS_JOB_POD=$(oc get pods --namespace=openshift-insights -o custom-columns=:metadata.name --no-headers --field-selector=status.phase=Running --selector job-name=insights-operator-job)                                                                                                 
while [ -z "$INSIGHTS_JOB_POD" ] ; do
  echo -n "."
  sleep 5
  COUNTER=$((COUNTER - 1))
  [ $COUNTER -eq 0 ] && echo "Timed out running $INSIGHTS_JOB_POD" && exit 1
  INSIGHTS_JOB_POD=$(oc get pods --namespace=openshift-insights -o custom-columns=:metadata.name --no-headers --field-selector=status.phase=Running --selector job-name=insights-operator-job)                                                                                               
done
echo

[ -z "$INSIGHTS_JOB_POD" ] && echo "Still no insights-operator-job started" && exit 1

# Check if job ran successfull
oc logs -n openshift-insights $INSIGHTS_JOB_POD insights-operator | grep -qE "Wrote [0-9]+ records to disk in" || echo "There was an error running the job" || exit 1                                                                                                                        

echo "Data collected, copying to local machine for upload (to ./insights-data)"
oc cp openshift-insights/$INSIGHTS_JOB_POD:/var/lib/insights-operator ./insights-data

echo "Uplodaing data to cloud.redhat.com"
# insights-data is a directory containing all the collected data as tar per day, so loop over all files
for data in insights-data/*.tar.gz; do
  curl -s -H "User-Agent: insights-operator/one10time200gather184a34f6a168926d93c330 cluster/$CLUSTER_ID" \
         -H "Authorization: Bearer $TOKEN" \
         -F "upload=@$data; type=application/vnd.redhat.openshift.periodic+tar" \
         https://cloud.redhat.com/api/ingress/v1/upload
done

delete_job_and_pod

echo "Removing temporary data"
rm -rf ./insights-data

echo "Upload finished, check https://console.redhat.com/openshift/insights/advisor/clusters/$CLUSTER_ID for results"